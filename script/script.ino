#include "TFT_eSPI.h"
#include "RTC_SAMD51.h"
#include "DateTime.h"
 
TFT_eSPI tft;
RTC_SAMD51 rtc;

/*########################################################*/

DateTime now = DateTime(F(__DATE__), F(__TIME__));
short minute;
short hour;
short second = 0;

unsigned int actuel = 0; 
unsigned int visiteurs = 0; 
const unsigned short maxAffluence = 10;
int resetTime;

String minuteString;
String hourString;
String message;

/*########################################################*/



void midDrawing() {
  tft.drawString("affluence: " + String(actuel), 20, 125);
  tft.drawString("visiteurs: " + String(visiteurs), 20, 75);
}


void add(){
  visiteurs++;
  actuel++;

  if (actuel <= maxAffluence)
  {
    tft.fillRect(0,41,320,240,TFT_GREEN);
  }
  else
  {
    tft.fillRect(0,41,320,240,TFT_RED);
    if (actuel == maxAffluence + 1)
    {
      analogWrite(WIO_BUZZER, 128);
      delay(100000);
      analogWrite(WIO_BUZZER, LOW);
    }
  }
  midDrawing();
}


void substract(){
  if (actuel != 0)
  {
    actuel--;
  
    if (actuel <= maxAffluence)
    {
      tft.fillRect(0,41,320,240,TFT_GREEN);
      if (actuel == maxAffluence)
      {
        analogWrite(WIO_BUZZER, 128);
        delay(100000);
        analogWrite(WIO_BUZZER, LOW);
      }
    }
    else
    {
      tft.fillRect(0,41,320,240,TFT_RED);
    }
    midDrawing();
  }
}

void resetInit()
{
  resetTime = millis();
  Serial.println("RESETINIT");
}

void reset()
{
   Serial.println("RESET");
   if (millis() - resetTime <= 5000)
  {
    actuel = 0;
    visiteurs = 0;
    tft.fillRect(0,41,320,240,TFT_GREEN);
    midDrawing();
    resetTime = 0;
  }
}

void bar(){
  tft.fillRect(0,0,320,40,TFT_BLUE);
  tft.drawString("+  -   raz",5,10);
  if (now.minute() < 10)
      minuteString = "0" + String(minute);
    else
      minuteString = String(minute);
    if (now.hour() < 10)
      hourString = "0" + String(hour);
    else
      hourString = String(hour);
  
    tft.drawString(hourString + ":" + minuteString,230,10);
}

void setup() {
  Serial.begin(115200);
  
  rtc.adjust(now);
  now = rtc.now();
  second = now.second();
  minute = now.minute();
  hour = now.hour();
  
  tft.begin();
  tft.setRotation(3);
  tft.setTextColor(TFT_BLACK);
  tft.setTextSize(3);
  
  tft.fillScreen(TFT_GREEN);
  tft.drawString("Need serial com", 10, 10);
  while (!Serial){
    continue;
  }
  pinMode(WIO_KEY_A, INPUT_PULLUP);
  pinMode(WIO_KEY_B, INPUT_PULLUP);
  pinMode(WIO_KEY_C, INPUT_PULLUP);
  pinMode(WIO_5S_PRESS, INPUT_PULLUP);

  tft.fillRect(0,0,320,40,TFT_BLUE); // BANDEAU DU HAUT
  
  tft.drawString("+   -   raz",5,10);
  if (now.minute() < 10){
    minuteString = "0" + String(now.minute());
  }
  else{
    minuteString = String(now.minute());
  }
  if (now.hour() < 10){
    hourString = "0" + String(now.hour());
  }
  else{
    hourString = String(now.hour());
  }
  tft.drawString(hourString + ":" + minuteString,230,10);

  
  tft.fillRect(0,41,320,240,TFT_GREEN);
  midDrawing();

  attachInterrupt(WIO_KEY_C, add, FALLING);
  attachInterrupt(WIO_KEY_B, substract, FALLING);
  attachInterrupt(WIO_KEY_A, resetInit, FALLING);
  attachInterrupt(WIO_5S_PRESS, reset, FALLING);
}

void loop() {
  delay(1000);
  second++;
  if (second % 60 == 0)
  {
    minute++;
    if (minute % 60 == 0)
      hour++;
    if (hour % 24 == 0)
      hour = 0;
    bar();
  }
  if (Serial)
  {
    if (minute % 15 == 0 && second % 60 == 0)
    {
      Serial.println("----------");
      Serial.println("Heure : " + String(hour) + " : " + String(minute)); 
      Serial.println("Visiteurs : " + String(visiteurs));
      Serial.println("Affluence : " + String(actuel));
    }
    if (Serial.available() > 0)
    {
      message = Serial.readStringUntil('\n');
      tft.drawString("Nouveau message", 20, 175);
    }
  }

}